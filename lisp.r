# The Little LISPer https://archive.org/details/tlLISP

# Response to 1.1

typeof(quote(ATOM))

# Response to 1.2

typeof(quote(TURKEY))

# response to 1.3

typeof(quote(1492))
typeof(1492)

# response to 1.4

typeof(quote(`3TURKEYS`))

# response to 1.5

typeof(quote(L))

# response to 1.6

typeof(quote(ATOM()))

# response to 1.7

typeof(quote(ATOM(TURKEY, OR)))

# response to 1.8

typeof(quote(ATOM(TURKEY), OR)) # reports an error from `quote`

# response to 1.9

typeof(quote(ATOM(TURKEY)(OR)))

## R puts the operator of the list before the parens;
## whereas traditional LISP has the operator inside the parens.

# response to 1.10

typeof(expression(XYZ))

## R has a type `expression`, but probably the closest
## approximant to LISP S-EXP is `language`.

is.language(quote(XYZ))

# response to 1.11

is.language(quote(X(Y, Z)))     # TRUE

# response to 1.12

is.language(quote(X(Y)(Z)))

# response to 1.13

typeof(quote(HOW(ARE, YOU, DOING, SO, FAR)))

# response to 1.14

length(quote(HOW(ARE, YOU, DOING, SO, FAR)))    # 6

quote(HOW(ARE, YOU, DOING, SO, FAR))[[1]]       # HOW

## and so on

# response to 1.15

is.language(quote(HOW()(ARE)(YOU()(DOING(SO)), FAR)))   # TRUE

# response to 1.16

length(quote(HOW()(ARE)(YOU()(DOING(SO)), FAR)))        # 3

sapply(quote(HOW()(ARE)(YOU()(DOING(SO)), FAR)), identity)

## HOW()(ARE), YOU()(DOING(SO)), FAR

## Using `(` instead of identity is even better for corrupting R
## programmers.

# response to 1.17

length(list())  # 0

as.call(list()) # error from `as.call`

## In R, a `language` (which is what `as.call` creates)
## cannot have 0 elements

length(expression())    # 0

# Page 2

# response to 1.18

quote(NULL(NULL, NULL, NULL))   # haha

# response to 1.19

quote(A(B, C))[[1]]     # CAR was quite tricky to find

# response to 1.20

quote(A(B, C)(X, Y, Z))[[1]]    # even better than response 1.16

# response to 1.21

quote(HOTDOG)[[1]]      # An error (aligning with LISP tradition)

# response to 1.22

NULL[[1]]               # NULL !! Disturbingly !!

expression()[[1]]       # An error

# Principle No. 1

## CAR is spelled `[[1]]` in R and works on `language` `expression`
## and even `NULL` objects.

# response to 1.23

quote(HOTDOGS()()(AND(), PICKLE(), RELISH))[[1]]

# response to 1.24

## This is a reification of CAR the concept to CAR the callable function.
## We are already well into our R-reification.

# response to 1.25

quote(HOTDOGS()()(AND(), PICKLE(), RELISH))[[1]][[1]]

# response to 1.26

quote(A(B, C))[-1]      # B(C)  ; Note how A(B, C) --CDR-> B(C)

## Note change from [[ (for CAR) to [ (for CDR)

# response to 1.27

quote(A(B, C)(X, Y, Z))[-1]     # X(Y, Z)

# response to 1.28

quote(X()(T, R))[-1]

# response to 1.29

quote(HOTDOGS)[-1]      # An error (aligning with LISP tradition)

# response to 1.30

NULL[-1]                # NULL !! Disturbingly !!

expression()[-1]        # expression()  ?? Intriguingly ??

# Principle No. 2

## CDR is spelled `[-1]` in R (it can be thought of as selecting
## everything but the first element), it works on `language`
## objects as well as `NULL`.

# response to 1.31

quote(B()(X(Y), C()()))[-1][[1]]

# Page 3

# response to 1.32

quote(B()(X(Y),C()()))[-1][-1]  # (C()())()

## Worth noting R's exact response here. `C()()()` and
## `(C()())()` represent the same call (`language` object).
## Interestingly, they parse differently, when using R's parse function.

# response to 1.33

quote(A(B(C()), D))[[1]][-1]    # An error, as expected

# response to 1.34

## L must be a list; in R NULL is valid!

# response to 1.35

## L must be a list; in R NULL is valid!

# response to 1.36

as.call(pairlist(quote(A), quote(BUTTER(AND, JELLY))))

## We are flexing R quite hard here. It may be better
## to wrap CONS, FIRST, REST

## I think that "sticks ... onto the front of the list" is better than
## "inserts at the font".

# response to 1.37

## my currently best definitions of CONS, CAR, CDR, are:

cons <- function(car, cdr) as.call(c(car, as.list(cdr)))

car <- function (cons) cons[[1]]

cdr <- function (cons) cons[-1]

cons(quote(MAYONNAISE(AND)), quote(PEANUT(BUTTER, AND, JELLY)))

# response to 1.38

cons(quote(HELP()(THIS)), quote(IS(VERY, HARD()(TO, LEARN))))

## it seems even harder with R's notation for lists, which
## puts the operator element before the (

# response to 1.39

cons(quote(A), quote(B)) # same as cons(quote(A), quote(B())

## Note that this... not LISPy

# response to 1.40

cons(quote(A(B, C())), NULL)    # Can't use () in R

# response to 1.41

cons(quote(A), NULL)    # A()

# response to 1.42

cons(quote(A(B, C())), quote(B))        # evaluates; see 1.39

## sorry

## Response to footnote "prevent you from thinking about CONSing
something onto an atom": ... except for NIL. (they haven't
introduced the NIL = () idea yet).

# response to 1.43

## L must be a list (but not in R)

# Principle No. 3

## The second argument of (CONS S L) must be a list (or not), and
## the result mut also be a list.


# response to 1.44

cons(quote(A), car(quote(B()(C, D))))   # because (CAR L) is (B)

# response to 1.45

cons(quote(A), cdr(quote(B()(C, D))))   # because (CDR L) is (C D)

## This and the previous prompt are illustrating using CAR and
CDR for destructuring.

# Page 4

# END
